import serial
import math
import re
import time
from IPOSSensorValueTransmitter import connect_mqtt, create_rawdata_msg, sendMessage
# setups
position_3383 = [1.25,5.89,1.06]
position_885  = [0,0,0.97]
position_2934 = [4.27,1.33,1]
position_1107 = [4.24,5.83,1.05]
height_tag = 0
the_hostname = "UWB_rasp"
mqttServer = "192.168.0.143"
mqttPort = 1883
#MQTT_SERVER = "broker.hivemq.com"
sensorId = "UWB1"
topic = "usertopic/SensorEventWrapper"
beaconId = ["3383", "885", "2934", "1107"]


ser = serial.Serial('/dev/ttyUSB0', 115200, timeout=1)  # open serial port
print(ser.name)         # check which port was really used



def extractNumbersFromLine(line):
    return re.findall(r'-?\d+\.?\d*',line)

def get_distance(number_found):
    return number_found[1], number_found[3]
    
def average(lst):
    return sum(lst) / len(lst)

def read_serial(number):
    d1 = []
    d2 = []
    d3 = []
    d4 = []
    print("read started")
    with serial.Serial('/dev/ttyUSB0', 115200, timeout=1) as ser:
        for x in range(number):
            line = ser.readline()   # read a '\n' terminated line
            #print(len(line))
            if (len(line) <= 30):
                print("reseting serial...")
                ser.close()
                ser.open()
                line = ser.readline()
            #print(line)
            number_found = extractNumbersFromLine(line.decode("utf-8"))
            if(len(number_found)>=4):
                tag, distance = get_distance(number_found)
                #print(tag, distance)
                if (tag == "3383"):
                    distance_xy = pow(float(distance),2)-pow((position_3383[2]-height_tag),2)
                    if (distance_xy >= 0):
                        distance = math.sqrt(distance_xy)
                        d1.append(float(distance))
                elif (tag == "885"):
                    distance_xy = pow(float(distance),2)-pow((position_885[2]-height_tag),2)
                    if (distance_xy >= 0):
                        distance = math.sqrt(distance_xy)
                        d2.append(float(distance))
                elif (tag == "2934"):
                    distance_xy = pow(float(distance),2)-pow((position_2934[2]-height_tag),2)
                    if (distance_xy >= 0):
                        distance = math.sqrt(distance_xy)
                        d3.append(float(distance))
                elif (tag == "1107"):
                    distance_xy = pow(float(distance),2)-pow((position_1107[2]-height_tag),2)
                    if (distance_xy >= 0):
                        distance = math.sqrt(distance_xy)
                        d4.append(float(distance))
    if (len(d1) == 0 or len(d2) == 0 or len(d3) == 0 or len(d4) == 0):
        print ("sth went wrong")
        return [-1, -1, -1, -1]
    return average(d1), average(d2), average(d3), average(d4)

# create the client
uwbValueTransmitter = IPOSUwbValueTransmitter.__init__(mqttServer, mqttPort,"UWB")

# enable TLS
#client.tls_set(tls_version=mqtt.ssl.PROTOCOL_TLS)
#client.username_pw_set("phone", "IPOSframework123")
#client.connect(MQTT_SERVER, 1883)
#client.loop_start()
#client.subscribe(MQTT_PATH)



starttime = time.time()
line = ser.readline()
while True:
    distance = read_serial(60)
    if (distance == [-1, -1, -1, -1]):
        continue
    uwbValueTransmitter.send_rawdata_msg(sensorId, beaconId, distance)
    time.sleep(1-((time.time() - starttime)%1))

