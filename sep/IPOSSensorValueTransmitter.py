# -*- coding: utf-8 -*-
"""
Created on Wed Jan 26 09:52:38 2022

@author: s3340992
"""
from error import *
import math
import numpy as np
import logging
import re
import datetime
import time
import json
import paho.mqtt.client as mqtt

# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    if rc == 0:
        print("Connected successfully")
    else:
        print("Connect returned result code: " + str(rc))

# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):
    print("Received message: " + msg.topic + " -> " + msg.payload.decode("utf-8"))

class IPOSSensorValueTransmitter:
	__init__(self, mqttServer, mqttPort, sensorTyp):
		# create the client
        self.client = mqtt.Client()
        self.client.on_connect = on_connect
        self.client.on_message = on_message
        self.client.connect(mqttServer, mqttPort)
		self.sensorType = sensorTyp
	
	
class IPOSUwbSensorValueTransmitter(IPOSSensorValueTransmitter):
    def send_rawdata_msg(client, sensorId, beaconId):
        timestamp = str(datetime.datetime.now().date())
        timestamp += "T"
        timestamp += str(datetime.datetime.now().time())
        timestamp += "+00:00"
        #print(timestamp)
        
        #Check the length of list_beacon_id and list_distance, they must be the same
        if listBeaconId.size() != listDistance.size():
            raise RawDataSizeError
        
        distance_msg = {
            listBeaconId[0] : listDistance[0],
            }
        
        for i in range(len(listBeaconId)-1):
            distance_msg[listBeaconId[i+1]] = listDistance[i+1]
    
        json_msg = {
            "timestamp" : timestamp,
            "sensorId" : sensorId,
            "distances" : distance_msg
            }
        wrapper_msg = {"uwbRawDataEvent" : [json_msg]
            }
        msg = json.dumps(wrapper_msg)
        #print(msg)
        self.client.publish(topic, msg)


    def create_position_msg(sensorId, x, y, z, accuracy, refSystemId, ori_x, ori_y, ori_z, ori_w):
        #print(d[0])
        timestamp = str(datetime.datetime.now().date())
        timestamp += "T"
        timestamp += str(datetime.datetime.now().time())
        timestamp += "+00:00"
        #print(timestamp)
    
        point_msg = {
            "x" : x,
            "y" : y,
            "z" : z
            }
    
        position_msg = {
            "refSystemId" : refSystemId,
            "point" : point_msg,
            "accuracy" : accuracy
            }
        orientation_msg = {
            "x" : ori_x,
            "y" : ori_y,
            "z" : ori_z,
            "w" : ori_w
            }
    
        json_msg = {
            "sensorId" : sensorId,
            "position" : position_msg,
            "orientation" : orientation_msg,
            "lastPosUpdate" : timestamp
            }
        wrapper_msg = {"sensorPositionEvent" : [json_msg]
            }
        msg = json.dumps(wrapper_msg)
        print(msg)
        return msg
    

    
    def connect_mqtt(mqttServer, mqttPort):
        # create the client
        client = mqtt.Client()
        client.on_connect = on_connect
        client.on_message = on_message
        client.connect(mqttServer, mqttPort)
        return client
    

class IPOSUwbValueTransmitter(IPOSSensorValueTransmitter):
	def __init__():
	
	def 
        
        
        
    
