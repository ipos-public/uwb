# -*- coding: utf-8 -*-
"""
Created on Wed Jan 26 15:44:00 2022

@author: s3340992
"""

# define Python user-defined exceptions
class Error(Exception):
    """Base class for other exceptions"""
    pass

class RawDataSizeError(Error):
    """Raised when the input raw data size is not consistent"""
    pass